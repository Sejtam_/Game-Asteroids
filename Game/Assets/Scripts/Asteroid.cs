﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    float t;
    Vector3 startPosition;
    Vector3 endPossition;
    Vector3 target;
    float currentLerpTime = 0;
    float lerpTime = 1;
    GameManager gm;
    int Position;
    bool Move = false;
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        Position = Random.Range(0, gm.Positions.Length);
        endPossition = gm.Positions[Position].position;
        //startPosition = new Vector3(transform.position.x,transform.position.y,-5);
        startPosition = transform.position;
        lerpTime = Random.Range(3,6);
    }

    private void Update()
    {
        //transform.position = new Vector3(transform.position.x, transform.position.y, -5);
        endPossition = gm.Positions[Position].position;
        currentLerpTime += Time.deltaTime;
        if(currentLerpTime >= lerpTime)
        {
             Destroy(gameObject);
        }
        float Perc = currentLerpTime / lerpTime;
        transform.position = Vector2.Lerp(startPosition, endPossition, Perc);
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("ASTEROID_COLLISION: " + other.collider.gameObject.tag);
        if (other.collider.gameObject.tag == "Player")
        {
            Destroy(other.collider.gameObject);
            gm.End_Game();
        }
    }
    /*public IEnumerator MoveToPos(Vector3 endPos)
    {
        Vector3 startPos = transform.position;
        float t = 0f;
        while (t < 1f)
        {
            transform.position = Vector3.Lerp(startPos, endPos, t);
            t += Time.deltaTime;
        }
        Destroy(gameObject);
        yield return null;
    }*/
}
