﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movment : MonoBehaviour
{

    public float speed = 10;
    private float privatespeed;
    private Vector2 direction;
    public Vector3 mousePosition;
    public GameManager gm;
    protected Joystick joystick;
    float angle;
    // Start is called before the first frame update
    void Start()
    {
        joystick = FindObjectOfType<Joystick>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        //Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y, Camera.main.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        if (joystick != null) {
            if (joystick.Horizontal > 0)
            {
                transform.Translate(Vector2.up * speed * Time.deltaTime);
            }
            /*else if (joystick.Horizontal < 0)
            {
                transform.Translate(-Vector2.up * speed * Time.deltaTime);
            }
            if (joystick.Vertical > 0)
            {
                transform.Translate(-Vector2.right * speed * Time.deltaTime);
            }
            else if (joystick.Vertical < 0)
            {
                transform.Translate(new Vector3(1,0,0) * speed * Time.deltaTime);
            }*/
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(new Vector3(0, 1, 0) * speed * Time.deltaTime);
        }
        /*else if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(-new Vector3(0, 1, 0) * speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(-new Vector3(1, 0, 0) * speed * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(new Vector3(1, 0, 0) * speed * Time.deltaTime);
        }*/

        /*if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(Vector2.up * 50 * Time.deltaTime, Space.World);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(-Vector2.up * 50 * Time.deltaTime, Space.World);
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(-Vector2.right * 50 * Time.deltaTime, Space.World);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                //transform.position = Vector2.Lerp(transform.position, -Vector2.right * 50 * Time.deltaTime, 0.6f);
                transform.Translate(Vector2.right * 50 * Time.deltaTime, Space.World);
            }
        }*/
        //Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y, Camera.main.transform.position.z);
        mousePosition = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, transform.position.y);
        direction = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);
        angle = -Mathf.Atan2(direction.x,direction.y) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0,0, angle));
        //mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Debug.DrawLine(transform.position, mousePosition, Color.cyan);
        if (Input.GetMouseButtonDown(0)) { Shoot(); }
        
    }

    private void Shoot()
    {
         /*Debug.DrawRay(transform.position, direction, Color.red, 100000000f);
         RaycastHit2D hit = Physics2D.Raycast(transform.position, direction);
         if(hit.collider != null)
         {
             Debug.Log(hit.collider.gameObject.name);
             Destroy(hit.collider.gameObject);
         }
         else
         {
             Debug.Log("Nic");
         }*/

        GameObject strela = Instantiate(gm.strela,transform.position,Quaternion.identity);
        strela.GetComponent<Strela>().endPosition = direction;
        strela.GetComponent<Strela>().setAngle(angle);

    }
}
