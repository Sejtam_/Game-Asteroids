﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Strela : MonoBehaviour
{
    Vector3 startPosition;
    public Vector3 endPosition;
    public float Angle;
    private GameManager gm;
    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        endPosition = startPosition;
    }

    public void setAngle(float angle)
    {
        Angle = angle;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, Angle+90));
    }

    private void Update()
    {
        transform.Translate(transform.right * 15 * Time.deltaTime, Space.World);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("Colission    Tag:" + other.gameObject.tag);
        if (other.collider.gameObject.tag == "Asteroid")
        {
            gm.score += 1;
            Destroy(other.collider.gameObject);
            Destroy(gameObject);
        }
    }
}
