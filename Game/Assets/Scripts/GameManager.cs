﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    bool spawn = true;


    public Transform[] Positions;
    public GameObject[] Prefabs;
    public GameObject strela;
    public GameObject asteroids_spawn;
    public TextMeshProUGUI text;
    public GameObject end_text;
    public GameObject end_score_text;
    public GameObject end_button;
    public GameObject restart_button;

    public int score;

    public void Start()
    {
        StartCoroutine(CreateAsteroid());
        score = 0;
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("MainMenu",LoadSceneMode.Single);
        }

        text.text = "Score: " + score;
    }

    IEnumerator CreateAsteroid()
    {
        if (spawn == true)
        {
            GameObject asteroid = Instantiate(Prefabs[Random.Range(0, Prefabs.Length)], Positions[Random.Range(0, Positions.Length)]);
            asteroid.transform.parent = asteroids_spawn.transform;
            yield return new WaitForSeconds(0.15f);
            StartCoroutine(CreateAsteroid());
        }
    }

    public void End_Game()
    {
        spawn = false;
        text.gameObject.SetActive(false);
        end_score_text.SetActive(true);
        end_text.SetActive(true);
        end_button.SetActive(true);
        restart_button.SetActive(true);
        end_score_text.GetComponent<TextMeshProUGUI>().text = "Score: " + score;
        GameObject[] obj = GameObject.FindGameObjectsWithTag("Asteroid");
        foreach(GameObject o in obj)
        {
            Destroy(o);
        }
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }

    public void Game()
    {
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }
}
